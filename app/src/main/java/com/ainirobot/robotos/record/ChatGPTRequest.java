package com.ainirobot.robotos.record;

import java.util.ArrayList;
import java.util.List;

public class ChatGPTRequest {
    private String model;
    private List<Message> messages;
    private int max_tokens;

    // Constructor with system and user messages
    public ChatGPTRequest(String model, String systemMessageContent, List<Message> userMessages, int max_tokens) {
        this.model = model;
        this.messages = new ArrayList<>();
        if (systemMessageContent != null && !systemMessageContent.isEmpty()) {
            this.messages.add(new Message("system", systemMessageContent)); // Add system message if provided
        }
        this.messages.addAll(userMessages); // Add all user messages
        this.max_tokens = max_tokens;
    }

    // Constructor with only user messages
    public ChatGPTRequest(String model, List<Message> userMessages, int max_tokens) {
        this.model = model;
        this.messages = new ArrayList<>(userMessages);
        this.max_tokens = max_tokens;
    }

    // Getters and Setters

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public int getMaxTokens() {
        return max_tokens;
    }

    public void setMaxTokens(int max_tokens) {
        this.max_tokens = max_tokens;
    }

    // Inner class to represent a message
    public static class Message {
        private String role;
        private String content;

        public Message(String role, String content) {
            this.role = role;
            this.content = content;
        }

        // Getters and Setters

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
