package com.ainirobot.robotos.record;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.ainirobot.robotos.R;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.speech.v1.RecognitionAudio;
import com.google.cloud.speech.v1.RecognitionConfig;
import com.google.cloud.speech.v1.RecognizeResponse;
import com.google.cloud.speech.v1.SpeechClient;
import com.google.cloud.speech.v1.SpeechRecognitionResult;
import com.google.cloud.speech.v1.SpeechSettings;
import com.google.cloud.texttospeech.v1.AudioConfig;
import com.google.cloud.texttospeech.v1.AudioEncoding;
import com.google.cloud.texttospeech.v1.SsmlVoiceGender;
import com.google.cloud.texttospeech.v1.SynthesisInput;
import com.google.cloud.texttospeech.v1.SynthesizeSpeechResponse;
import com.google.cloud.texttospeech.v1.TextToSpeechClient;
import com.google.cloud.texttospeech.v1.TextToSpeechSettings;
import com.google.cloud.texttospeech.v1.VoiceSelectionParams;
import com.google.protobuf.ByteString;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ChatGptCallback {
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private static final String[] PERMISSIONS = {Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private MediaRecorder recorder;
    private String fileName;
    private boolean permissionToRecordAccepted = false;
    private TextView view_chat_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ar_voice);

        // Request permissions
        ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_RECORD_AUDIO_PERMISSION);

        // Initialize UI elements
        Button recordButton = findViewById(R.id.recordButton);
        Button stopButton = findViewById(R.id.stopButton);
        Button playButton = findViewById(R.id.playButton);
        view_chat_result = findViewById(R.id.view_chat_result);

        recordButton.setOnClickListener(v -> startRecording());
        stopButton.setOnClickListener(v -> stopRecording());
        playButton.setOnClickListener(v -> {
            playAudio();
        });
    }

    private void startRecording() {
//        fileName = getExternalCacheDir().getAbsolutePath() + "/audio.3gp";
        fileName = getExternalCacheDir().getAbsolutePath() + "/audio.mp3";
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(fileName);
        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e("MainActivity", "prepare() failed", e);
        }
        recorder.start();
        Toast.makeText(this, "Recording started", Toast.LENGTH_SHORT).show();
    }

    private void stopRecording() {
        recorder.stop();
        recorder.release();
        recorder = null;
        Toast.makeText(MainActivity.this, "Recording stopped", Toast.LENGTH_SHORT).show();

        // Start the transcription and text-to-speech process
        try {
            String text = transcribeAudio();
//          Toast.makeText(this, "Response: " + responseText, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e("MainActivity", "Error in stopRecording", e);
            Toast.makeText(this, "Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private String transcribeAudio() throws Exception {
        try (InputStream stream = getResources().openRawResource(R.raw.test_cred)) {
            GoogleCredentials credentials = GoogleCredentials.fromStream(stream)
                    .createScoped(Collections.singletonList("https://www.googleapis.com/auth/cloud-platform"));

            SpeechSettings speechSettings = SpeechSettings.newBuilder()
                    .setCredentialsProvider(FixedCredentialsProvider.create(credentials))
                    .setEndpoint("speech.googleapis.com:443")
                    .build();

            try (SpeechClient speechClient = SpeechClient.create(speechSettings)) {
                int rawResourceId = R.raw.en; // e.g., audio_file.wav in res/raw/
                try {
                  ////  InputStream inputStream = getApplicationContext().getResources().openRawResource(R.raw.audio_ar);
                  ////  ByteString audioBytes = ByteString.readFrom(inputStream);

                    File audioFile = new File(fileName);
                    if (audioFile.exists()) {
                        try (InputStream audioStream = new FileInputStream(audioFile)) {
                     RecognitionAudio audio = RecognitionAudio.newBuilder()
                                    .setContent(ByteString.readFrom(audioStream))
                                    .build();

//                            RecognitionAudio.newBuilder()
//                                    .setContent(ByteString.readFrom(new FileInputStream(fileName)))
//                                    .build();
//
//
 //                                               .setEncoding(RecognitionConfig.AudioEncoding.LINEAR16)  // Specify MP3 encoding

                    RecognitionConfig config = RecognitionConfig.newBuilder()
                            .setSampleRateHertz(16000)
                            .setEncoding(RecognitionConfig.AudioEncoding.MP3)  // Specify MP3 encoding
                            .setLanguageCode("ar")        // Specify language code
                            .build();
                   //// RecognitionAudio audio = RecognitionAudio.newBuilder()
                   ////         .setContent(audioBytes)
                   ////         .build();
                    RecognizeResponse response = speechClient.recognize(config, audio);
                    System.out.println("Full Response: " + response);

                    List<SpeechRecognitionResult> results = response.getResultsList();
                    if (results != null && !results.isEmpty()) {
                        SpeechRecognitionResult result = results.get(0);
                        if (!result.getAlternativesList().isEmpty()) {
                            // Return the first transcription result

                            String responseText = sendTextToChatGPT(result.getAlternatives(0).getTranscript());


                            return result.getAlternatives(0).getTranscript();

                        } else {
                            return "No transcription alternatives found";
                        }
                    } else {
                        Toast.makeText(this, "Recording result empty ", Toast.LENGTH_SHORT).show();

                        Log.d("111111111111111111", "No recognition results found");

                        return "No recognition results found";
                    }

                } catch (Exception e) {
                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();

                    return "Unexpected error: " + e.getMessage();
                }
                    }

                } catch (Exception e) {
                    return "Unexpected error: " + e.getMessage();
                }
            }
        } catch (FileNotFoundException e) {
            return "File not found: " + e.getMessage();
        } catch (IOException e) {
            return "IOException: " + e.getMessage();
        } catch (Exception e) {
            return "Unexpected error: " + e.getMessage();
        }
        return "";

    }





    private String sendTextToChatGPT(String text) {
        ChatGptConvert chatGptConvert = new ChatGptConvert();
        Toast.makeText(MainActivity.this, "tttttttttttttt "+text, Toast.LENGTH_LONG).show();
        chatGptConvert.sendTextToChatGPT(text, this);
        return "";
    }

    private void textToSpeech(String text) throws Exception {
        Toast.makeText(MainActivity.this, "text to speech "+text, Toast.LENGTH_SHORT).show();
        try (InputStream stream = getResources().openRawResource(R.raw.test_cred)) {
            GoogleCredentials credentials = GoogleCredentials.fromStream(stream)
                    .createScoped(Collections.singletonList("https://www.googleapis.com/auth/cloud-platform"));
            TextToSpeechSettings ttsSettings = TextToSpeechSettings.newBuilder()
                    .setCredentialsProvider(FixedCredentialsProvider.create(credentials))
                    .setEndpoint("texttospeech.googleapis.com:443")
                    .build();

            try (TextToSpeechClient textToSpeechClient = TextToSpeechClient.create(ttsSettings)) {
                SynthesisInput input = SynthesisInput.newBuilder().setText(text).build();
                VoiceSelectionParams voice = VoiceSelectionParams.newBuilder()
                        //     .setLanguageCode("ar-SA") // Use the appropriate language code
                        .setLanguageCode("ar") // Use the appropriate language code
                        .setSsmlGender(SsmlVoiceGender.NEUTRAL)
                        .build();
                AudioConfig audioConfig = AudioConfig.newBuilder()
                        .setAudioEncoding(AudioEncoding.MP3)
                        .build();

//                RecognitionConfig config = RecognitionConfig.newBuilder()
//                        .setSampleRateHertz(16000)
//                        .setEncoding(RecognitionConfig.AudioEncoding.MP3)  // Specify MP3 encoding
//                        .setLanguageCode("ar")        // Specify language code
//                        .build();


                SynthesizeSpeechResponse response = textToSpeechClient.synthesizeSpeech(input, voice, audioConfig);

                // Use internal storage directory for file output
                File fileDir = getApplicationContext().getFilesDir();
                File outputFile = new File(fileDir, "output.mp3");

                // Write the audio contents to the file
                try (FileOutputStream out = new FileOutputStream(outputFile)) {
                    out.write(response.getAudioContent().toByteArray());
                }
            }
        }
    }

    private void playAudio() {
        MediaPlayer mediaPlayer = new MediaPlayer();
        try {
            // Get the path to the audio file
            File fileDir = getApplicationContext().getFilesDir();
            File audioFile = new File(fileDir, "output.mp3");
            String filePath = audioFile.getAbsolutePath();

            // Set data source and prepare mediaPlayer
            mediaPlayer.setDataSource(filePath);
            mediaPlayer.prepare();
            mediaPlayer.start();
            Toast.makeText(this, "Playing audio", Toast.LENGTH_SHORT).show();
        } catch (IOException | IllegalStateException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionToRecordAccepted = requestCode == REQUEST_RECORD_AUDIO_PERMISSION &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED;
        if (!permissionToRecordAccepted) finish();
    }


    @Override
    public void onResponse(String response) {
        // Update the TextView with the response on the main thread
        runOnUiThread(() -> view_chat_result.setText(response));
        try {
            textToSpeech(response);
        } catch (Exception e) {
            if (e.getMessage() != null)
                Log.e("errrrrrrrr", e.getMessage().toString());
        }
    }

    @Override
    public void onError(String errorMessage) {
        // Update the TextView with the error message on the main thread
        runOnUiThread(() -> view_chat_result.setText(errorMessage));
    }
}


