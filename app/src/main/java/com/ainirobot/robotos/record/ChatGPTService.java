// ChatGPTService.java
package com.ainirobot.robotos.record;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ChatGPTService {
    @Headers("Authorization: Bearer sk-proj-1zffPlIe1ALGHPzS5Gg4T3BlbkFJIWXM5esRStVdbNWFe0sA")
    @POST("v1/chat/completions")
    Call<ChatGPTResponse> sendTextToChatGPT(
//            @Header("Authorization") String authorizationHeader,
            @Body ChatGPTRequest request
    );
}