package com.ainirobot.robotos.record;

import android.util.Log;


import com.google.gson.Gson;

import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ChatGptConvert {

    private static final String TAG = ChatGptConvert.class.getSimpleName();
    private static final String BASE_URL = "https://api.openai.com/";

    // Replace with your OpenAI API key
    private static final String OPENAI_API_KEY = "sk-proj-1zffPlIe1ALGHPzS5Gg4T3BlbkFJIWXM5esRStVdbNWFe0sA";

    public void sendTextToChatGPT(String text, ChatGptCallback callback) {
        // Initialize Retrofit client
        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);

        // Create the OpenAI service
        ChatGPTService service = retrofit.create(ChatGPTService.class);

        // Prepare the request with only user messages
        ChatGPTRequest.Message userMessage = new ChatGPTRequest.Message("user", text);

        // Create the request object
        ChatGPTRequest chatRequest = new ChatGPTRequest("gpt-4", Collections.singletonList(userMessage), 150);

        // Log the JSON being sent
        Gson gson = new Gson();
        String jsonRequest = gson.toJson(chatRequest);
        Log.d(TAG, "Sending JSON: " + jsonRequest);

        // Call the API
        Call<ChatGPTResponse> call = service.sendTextToChatGPT(chatRequest);

        call.enqueue(new Callback<ChatGPTResponse>() {
            @Override
            public void onResponse(Call<ChatGPTResponse> call, Response<ChatGPTResponse> response) {
                if (response.isSuccessful()) {
                    ChatGPTResponse chatResponse = response.body();
                    if (chatResponse != null && !chatResponse.getChoices().isEmpty()) {
                        String reply = chatResponse.getChoices().get(0).getMessage().getContent();
                        Log.d(TAG, "Response: " + reply);
                        // Notify the callback with the response
                        callback.onResponse(reply);
                    } else {
                        Log.d(TAG, "No choices in the response.");
                        callback.onError("No choices in the response.");
                    }
                } else {
                    Log.e(TAG, "Request failed with code: " + response.code());
                    callback.onError("Request failed with code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<ChatGPTResponse> call, Throwable t) {
                Log.e(TAG, "API call failed", t);
                callback.onError("API call failed: " + t.getMessage());
            }
        });
    }
}
