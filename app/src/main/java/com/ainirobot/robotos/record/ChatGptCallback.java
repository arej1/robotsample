package com.ainirobot.robotos.record;

public interface ChatGptCallback {
    void onResponse(String response);
    void onError(String errorMessage);
}